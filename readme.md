# neoacademy.co.kr / 점검  

[![D0C70R_CH01](./d0c70r_ch01.png "H4CK3D I3Y D0C70R_CH01")](https://07001lab.tistory.com/)

```plaintext
H4CK3D I3Y 최박사 | D0C70R_CH01 | TWITTER @d0c70r_ch01 | DISCORD 최박사 | D0C70R_CH01#2885

22-04-22 ~ 22-04-29
```

---

## 정리  

[01 - 프론트엔드 코드 누출](#neoacademycokr--프론트엔드-코드-누출)  
[02 - 타임아웃과 로그인 풀림](#neoacademycokr--타임아웃과-로그인-풀림)  
[03 - 파일 업로드 취약점](#neoacademycokrreport--파일-업로드-취약점)  
[04 - 접근 권한 취약점](#neoacademycokrateliermain--접근-권한-취약점)  
[05 - 크롤링](#wwwneoacademycokrateliersearch--크롤링)  
[06 - CDN 에러](#wwwneoacademycokrateliersearch--cdn-에러)  
[07 - DB 에러메세지 노출](#wwwneoacademycokrateliersearch--db-에러메세지-노출)  
[08 - 권한 변조](#neoacademycokratlier--권한-변조)  
[09 - 난입](#neoeightstudionet--난입)  
[10 - 레거시 유출](#neoeightstudionetundefined--레거시-유출)  
[11 - 아이피 주소로 접근 가능](#classroom-serverneoacademycokr--13125186109)  
[12 - whois](#whios)  
[13 - nmap](#nmap)  

**거의 모든 기능이 서버에서 더블체크를 하고 있어서 공격이 힘듬**  
그러나 **피일 업로드 취약점**이 존재하고, 프론트엔드 코드의 **주석까지 다 노출**되어 있으며, **일부 중요한 정보들이 프론트엔드에 하드코딩** 되어있는 등의 문제가 존재함  
또한 **DB 에러메세지가 노출**되기 때문에 Error Based SQL Injection 에 대한 위험도 존재함  
테스트를 위해 **DoS 공격**을 클래스룸 서버와 메인페이지 두곳에 시도해 보았으나 어느정도 **내성이 있는것**으로 보임  
허나 **DDoS 나 DrDoS 공격의 경우는 알 수 없음**  

---

## neoacademy.co.kr/* / 프론트엔드 코드 누출  

![난독화1](./img/%EB%82%9C%EB%8F%85%ED%99%941.PNG "트리")  
![난독화2](./img/%EB%82%9C%EB%8F%85%ED%99%942.PNG "코드")  
주석까지 그대로 노출됨  
**난독화 작업 필요함**  

---

## neoacademy.co.kr/* / 타임아웃과 로그인 풀림  

![타임아웃](./img/cookie1.PNG "타임아웃시")  
![쿠키](./img/cookie2.PNG "쿠키는 그대로 남아있음")  
![F5](./img/cookie3.PNG " F5 누르면 복구")  
타임아웃시 로그인이 풀리는게 **의도된 것이라면 F5를 눌러 쿠키로 저장된 토큰으로 로그인된 상태로 되돌아갈 수 있음**  
**의도되지 않은 것이라면 수정 필요**  

---

## neoacademy.co.kr/report / 파일 업로드 취약점  

![위치](./img/0000.PNG "발생 위치")  
**파일 업로드** 기능 이용  

![파일](./img/0001.PNG "파일 업로드")  
![xss.png의 내용](./payload/xss.png "payload")  

```html
<script>alert("X55 I3Y D0C70R_CH01");</script>
```

[xss.png.html 로 이동](./payload/xss.png.html)  

![확장자 변조](./img/0002.PNG "확장자 변경")  
웹 프록시 툴을 이용 **확장자**를 **html** 로 변경함  

![업로드2](./img/0003.PNG "업로드 성공")  
![업로드2](./img/0004.PNG "위치 반환")  
**정상적으로 업로드에 성공함**

![파일 확인](./img/0005.PNG "CDN")  
파일 위치를 찾아내 직접 확인하여보았으나 CDN의 설정 때문인지 이미지로 읽어옴  

![업로드한 파일 내용 확인](./img/0006.PNG "변경점 없음")  
**파일 내용에는 변경점이 없음**  

**.exe** 같은 확장자 같은 경우는 확인해보지 않았지만 충분히 업로드될 가능성이 있음  
**업로드 시도하는 파일에 대한 content-type 과 파일 확장자에 대한 서버 단위의 더블체크 필요**  

---

## neoacademy.co.kr/atelier/main / 접근 권한 취약점  

![권한1](./img/perm1.PNG "권한 없는 상태")  
수강중이지 않은 상태임  

![권한2](./img/perm2.PNG "접근 불가")  
수강중이지 않은 상태이므로 아틀리에에 접근이 불가능함  

![권한3](./img/perm3.PNG "res 변조")  
서버의 **응답을 변조**시  

![권한4](./img/perm4.PNG "접근")  
**아틀리에 접근 가능**  

![권한5](./img/perm5.PNG "403")  
서버에서는 권한이 존재하지 않아서 **모든 데이터에 403**이 발생함  

---

## www.neoacademy.co.kr/atelier/search / 크롤링  

![크롤링1](./img/craw1.PNG "검색 시도")  
![크롤링2](./img/craw2.PNG "request")  
클래스룸 서버에 쿼리를 보냄

![크롤링3](./img/craw3.PNG "크롤링")  
**limit에 큰 수를 넣으면 크롤링이 가능함**  
22:04:29 19:37 기준 크롤링된 글 목록은 **list.json** 파일에 넣어둠 (788KB)  
[list.json 으로 이동](./list.json)  

---

## www.neoacademy.co.kr/atelier/search / CDN 에러  

![CDN1](./img/cdn1.PNG "널 문자")  
**널 문자 (0x00) 입력시 CDN에서 400 띄움**  

![CDN2](./img/cdn2.PNG "0x80")  
**(%01 - %79) 이외의 문자를 집어넣으면 InvalidURL 에러가 발생함 (HTTP 400)**  

---

## www.neoacademy.co.kr/atelier/search / DB 에러메세지 노출  

![DB](./img/db.PNG "db 에러메세지 노출")  
**검색 쿼리의 page 에 -1 을 집어넣으면 400에러와 함꼐 DB에러메세지가 노출됨**  
**Error Based SQL Injection 공격에 대한 위험이 있음**  

## neoacademy.co.kr/atlier/* / 권한 변조  

![변조1](./img/atelier01.PNG "수강중")  
수강중인 상태임  

![변조2](./img/atelier02.PNG "변조")  
type을 변조하여 manager 로 변경함  

![변조3](./img/atelier03.PNG "수강중 제거")  
이름 뒤의 (수강중) 이 제거됨  

![변조4](./img/atelier04.PNG "글쓰기")  
이상태로 아틀리에에 진입하면 **글쓰기 메뉴가 추가**됨  

![변조5](./img/atelier05.PNG "시도1")  
![변조6](./img/atelier06.PNG "시도2")  
글쓰기를 위해 파일 업로드를 시도해보았으나 **403으로 막힘**  

![변조7](./img/atelier07.PNG "글 페이지로 접근")  
글 페이지로 접근하게 되면 수정과 삭제버튼이 추가되어 있음  

![변조8](./img/atelier08.PNG "수정 시도")  
글 수정 시도시  

![변조9](./img/atelier09.PNG "실패?")  
![변조10](./img/atelier10.PNG "성공?")  
업로드에 실패했습니다와 성공했습니다 팝업이 동시에 뜸  

![변조11](./img/atelier11.PNG "실패")  
**글 내용은 변경되지 않음**  

![변조12](./img/atelier12.PNG "삭제 시도")  
글 삭제 시도시 비밀번호가 있음  

![변조13](./img/atelier13.PNG "하드코딩")  
**비밀번호가 프론트에** ***하드코딩*** **되어있음**  

![변조14](./img/atelier14.PNG "성공?")  
게시글이 삭제되었습니다 팝업이 뜸  

![변조15](./img/atelier15.PNG "실패")  
**삭제되지 않음**  

---

## neo.eightstudio.net/* / 난입  

![난입1](./img/%EB%82%9C%EC%9E%851.PNG "res 변조")  
수강중인 상태에서 type을 변조하여 manager 로 변경할시  

![난입2](./img/%EB%82%9C%EC%9E%852.PNG "난입")  
**수업중 난입** 가능  

원래 자신의 시간이 아닌 경우에도 가능함  

---

## neo.eightstudio.net/undefined / 레거시 유출  

![레거시1](./img/legacy01.PNG "res 변조")  
수강중인 상태에서 type을 변조하여 teacher로 변경할시  

![레거시2](./img/legacy02.PNG  "나의 수업")  
**나의 수업 항목의 이미지가 사라짐**  

![레거시3](./img/legacy03.PNG "undefined")  
입장하기 클릭시 **/undefined** 방으로 이동함  

![레거시4](./img/legacy04.PNG "난독화 안됨1")  
![레거시5](./img/legacy05.PNG "난독화 안됨2")  
역시나 이곳에서도 **난독화되지 않은 코드**를 찾을수 있었음  

![레거시6](./img/legacy06.PNG "/report/error")  
**/report/error** 페이지로 접근할시  

![레거시7](./img/legacy07.PNG "비밀번호")  
**비밀번호**를 입력하라고 하지만  

![레거시8](./img/legacy08.PNG "하드코딩")  
**비밀번호가 프론트에** ***하드코딩*** **되어있음**  

![레거시9](./img/legacy09.PNG "리포트")  
에러 제보 페이지에 **접근 가능**함  

![레거시10](./img/legacy10.PNG "검색")  
에러 페이지의 입력하세요 부분에  무언가를 **입력하려고 시도**하면  

![레거시11](./img/legacy11.PNG "blank")  
**화면이 하얘짐**  

![레거시12](./img/legacy12.PNG "테스트용")  
그 외에도 **테스트용 페이지가 삭제되지 않고 존재**함  

![레거시13](./img/legacy13.PNG "에러코드")  
에러코드가 존재는 하나 1000번 밖에 없음  

![레거시14](./img/legacy14.PNG "blank")  
/error/1000 으로 접근하면 화면이 하얘짐  
**테스트목적의 코드나 레거시 코드들은 존재해서 보안에 도움될 요소가 하나도 없음**  

---

## classroom-server.neoacademy.co.kr / 13.125.186.109  

![서버1](./img/classroom-server1.PNG "URL")  
![서버2](./img/classroom-server2.PNG "IP")  
**IP 주소로 접근 가능함**  
**http 프로토콜로 접근 가능함**  

---

## whios  

```plaintext
C:\cmder
λ  whois neoacademy.co.kr

Whois v1.21 - Domain information lookup
Copyright (C) 2005-2019 Mark Russinovich
Sysinternals - www.sysinternals.com

Connecting to KR.whois-servers.net...

query : neoacademy.co.kr


# KOREAN(UTF8)

도메인이름                  : neoacademy.co.kr
등록인                      : 노진
책임자                      : 노진
책임자 전자우편             : mrn5374@naver.com
등록일                      : 2022. 02. 22.
최근 정보 변경일            : 2022. 02. 22.
사용 종료일                 : 2027. 02. 22.
정보공개여부                : N
등록대행자                  : (주)후이즈(http://whois.co.kr)
DNSSEC                      : 미서명

1차 네임서버 정보
   호스트이름               : ns-794.awsdns-35.net

2차 네임서버 정보
   호스트이름               : ns-1389.awsdns-45.org
   호스트이름               : ns-413.awsdns-51.com
   호스트이름               : ns-2043.awsdns-63.co.uk

네임서버 이름이 .kr이 아닌 경우는 IP주소가 보이지 않습니다.


# ENGLISH

Domain Name                 : neoacademy.co.kr
Registrant                  : No Jin
Administrative Contact(AC)  : No Jin
AC E-Mail                   : mrn5374@naver.com
Registered Date             : 2022. 02. 22.
Last Updated Date           : 2022. 02. 22.
Expiration Date             : 2027. 02. 22.
Publishes                   : N
Authorized Agency           : Whois Corp.(http://whois.co.kr)
DNSSEC                      : unsigned

Primary Name Server
   Host Name                : ns-794.awsdns-35.net

Secondary Name Server
   Host Name                : ns-1389.awsdns-45.org
   Host Name                : ns-413.awsdns-51.com
   Host Name                : ns-2043.awsdns-63.co.uk


- KISA/KRNIC WHOIS Service -


Connecting to demy.co.kr...
�˷��� ȣ��Ʈ�� ����ϴ�.
​C:\cmder
λ
```

---

## nmap  

```plaintext
​C:\cmder
λ  nmap -sS -Pn -A neoacademy.co.kr
Starting Nmap 7.92 ( https://nmap.org ) at 2022-04-22 04:39 ���ѹα� ǥ�ؽ�
Nmap scan report for neoacademy.co.kr (99.86.207.114)
Host is up (0.0030s latency).
Other addresses for neoacademy.co.kr (not scanned): 99.86.207.50 99.86.207.126 99.86.207.31
rDNS record for 99.86.207.114: server-99-86-207-114.icn51.r.cloudfront.net
Not shown: 998 filtered tcp ports (no-response)
PORT    STATE SERVICE  VERSION
80/tcp  open  http     Amazon CloudFront httpd
|_http-server-header: CloudFront
|_http-title: Did not follow redirect to https://neoacademy.co.kr/
443/tcp open  ssl/http Amazon CloudFront httpd
| http-server-header:
|   AmazonS3
|_  CloudFront
|_http-title: \xEB\x84\xA4\xEC\x98\xA4 \xED\x81\xB4\xEB\x9E\x98\xEC\x8A\xA4\xEB\xA3\xB8
| ssl-cert: Subject: commonName=www.neoacademy.co.kr
| Subject Alternative Name: DNS:www.neoacademy.co.kr, DNS:neoacademy.co.kr
| Not valid before: 2022-03-15T00:00:00
|_Not valid after:  2023-04-13T23:59:59
Warning: OSScan results may be unreliable because we could not find at least 1 open and 1 closed port
OS fingerprint not ideal because: Missing a closed TCP port so results incomplete
No OS matches for host
Network Distance: 14 hops

TRACEROUTE (using port 80/tcp)
HOP RTT     ADDRESS
1   1.00 ms 172.16.4.1
2   2.00 ms □□.□□□.□□□.□□□
3   2.00 ms □□□.□□.□□.□□□
4   2.00 ms □□□.□□.□.□□□
5   2.00 ms □□□.□□.□.□□□
6   2.00 ms □□□.□□□.□□□.□□
7   4.00 ms □□.□□.□□□.□□
8   2.00 ms □□.□□.□□□.□□
9   ... 13
14  2.00 ms server-99-86-207-114.icn51.r.cloudfront.net (99.86.207.114)

OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 34.29 seconds
​C:\cmder
λ
```

---

---

---

---
